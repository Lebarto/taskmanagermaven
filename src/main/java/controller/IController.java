package controller;

import model.Task;

import java.util.Calendar;
import java.util.HashSet;

/**
 * Created by Александр on 25.10.2015.
 */
public interface IController {
    public void addTask(String name, Calendar date, String contacts, String description);
    public void deleteTask(Task task);

    public void saveTasks();
    public HashSet<Task> getJournal();
}

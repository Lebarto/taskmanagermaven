package controller;

import java.io.*;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;

import model.Journal;
import model.Task;
import org.apache.log4j.Logger;
/**
 * Created by Александр on 25.10.2015.
 */
public class Controller implements IController {
    public static final Logger LOG=Logger.getLogger(Controller.class);
    Journal journal;
    public Controller() throws IOException, ClassNotFoundException {
        Journal j;
        j=Journal.getInstance();
        j.setJournal(loadTasks());
        checkNotify cn = new checkNotify();
        Thread th = new Thread(cn);
        th.start();
    }
    @Override
    public void addTask(String name, Calendar date, String contacts, String description) {
        Task t = new Task(name,description,date,contacts);
        journal.addTask(t);

    }

    @Override
    public void deleteTask(Task task) {
        Task t = new Task();
        journal.deleteTask(t);
    }


    @Override
    public void saveTasks()  {
        try {
            FileOutputStream fos = new FileOutputStream("save.tm");
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(journal.getTasks());
            oos.flush();
            oos.close();
        }
        catch (IOException e){
            LOG.error("IOException");
        }

    }
    private HashSet<Task> loadTasks(){
        HashSet<Task> tasks=null;
        try {
            FileInputStream fis = new FileInputStream("temp.out");
            ObjectInputStream oin = new ObjectInputStream(fis);
           tasks = (HashSet<Task>) oin.readObject();
        }
        catch (IOException e){
            LOG.error("IOException");

        }
        catch (ClassNotFoundException c){
            LOG.error("ClassNotFoundException");
        }
        return tasks;
    }
    @Override
    public HashSet<Task> getJournal(){
        return journal.getTasks();
    }
    class checkNotify implements Runnable{

        @Override
        public void run() {
            while(true){

                for (Iterator iter = getJournal().iterator(); iter.hasNext();) {
                    Task t = (Task) iter.next();
                    //t.getName();
                }


            }
        }
    }


}

package model;

import java.io.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Nastya on 25.10.2015.
 */
public class Task implements Serializable{
    private String name;
    private String description;
    private Calendar date;
    private String contacts;

    public Task(){
        name = "New task";
        description = "-";
        date = new GregorianCalendar();
        contacts = "89179720354";
    }
    public Task(String name, String description, Calendar date, String contacts) {
        this.name = name;
        this.description = description;
        this.date = date;
        this.contacts = contacts;
    }
    public Task(String name, Calendar date){
        this.name=name;
        this.date=date;
        this.date.set(Calendar.MONTH,date.get(date.MONTH)-1);
        description="-";
        contacts="89179720354";
    }

    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }
    //    public Calendar getDate() {
//        return date;
//    }
    public String getContacts() {
        return contacts;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    //    public void setDate(Calendar date) {
//        this.date = date;
//    }
    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getDate(){
        int year = date.get(date.YEAR);
        int month = date.get(date.MONTH)+1;
        int day = date.get(date.DATE);
        int hour = date.get(date.HOUR);
        int minute = date.get(date.MINUTE);
        int second = date.get(date.SECOND);
        return year+"."+month+"."+day+" "+hour+":"+minute+":"+second;
    }
    public static void writeTask(Task task, Writer out){
        PrintWriter writer = new PrintWriter(out);
        writer.println(task.name);
        writer.println(task.description);
        writer.println(task.date.get(task.date.YEAR));
        writer.println(task.date.get(task.date.MONTH)+1);
        writer.println(task.date.get(task.date.DATE));
        writer.println(task.date.get(task.date.HOUR));
        writer.println(task.date.get(task.date.MINUTE));
        writer.println(task.date.get(task.date.SECOND));
        writer.println(task.contacts);
        writer.close();
    }
    public static Task readTask(Reader in) throws IOException {
        StreamTokenizer str = new StreamTokenizer(in);
        str.nextToken();
        String name = (String)str.sval;
        String description = "";
        str.nextToken();
        while (str.nextToken()!=StreamTokenizer.TT_EOL){
            description += str.sval;
        }

        int year = (int)str.lineno();
        int month = (int)str.lineno();;
        int day = (int)str.lineno();;
        int hour = (int)str.lineno();;
        int minute = (int)str.lineno();;
        int second = (int)str.lineno();;
        String contacts = (String)str.sval;
        return  new Task(name,description, new GregorianCalendar(year,month,day,hour,minute,second), contacts);
    }
}

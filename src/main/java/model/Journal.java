package model;

import java.util.HashSet;

/**
 * Created by Александр on 25.10.2015.
 */
public class Journal {
    HashSet<Task> Tasks;
    private static Journal instance;
    public void addTask(Task task){
        Tasks.add(task);
    }
    public HashSet<Task> getTasks(){
        return Tasks;
    }
    public boolean deleteTask(Task task){
        return Tasks.remove(task);
    }
    public void setJournal(HashSet<Task> tasks){

        this.Tasks = tasks;
    }
    private Journal(){
    }
    public static Journal getInstance(){
        if(instance==null){
            instance=new Journal();
        }
        return instance;
    }

}

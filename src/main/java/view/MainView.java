package view;

import controller.Controller;
import exception.TaskNotSelectedException;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Task;

public class MainView extends Application {

    private Controller controller;
    private ListView<Task> taskList;

    @Override
    public void start(Stage primaryStage) throws Exception {

        controller = new Controller();

        Button addButton = new Button();
        addButton.setText("Add task");
        addButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                EditTaskView editTaskView = new EditTaskView();
                editTaskView.display();
                addTask(editTaskView.getTask());
            }
        });

        Button removeButton = new Button();
        removeButton.setText("Remove task");

        taskList = new ListView<Task>();
        taskList.setMinWidth(380);
//        for (model.Task task : controller.getJournal()) {
//            taskList.getItems().add(task);
//        }

        removeButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    removeTask(taskList.getSelectionModel().getSelectedItem());
                } catch (TaskNotSelectedException e) {
                    AlertBox.display("Error", "Select task for deleting");
                }
            }
        });


        GridPane pane = new GridPane();
        pane.setHgap(50);
        pane.setVgap(5);
        pane.setPadding(new Insets(10));
        pane.add(taskList, 0, 0, 2, 1);
        pane.add(addButton, 0, 2);
        pane.add(removeButton, 2, 2);


        Scene mainScene = new Scene(pane, 390, 400);
        primaryStage.setScene(mainScene);
        primaryStage.setResizable(false);
        primaryStage.setTitle("model.Task Manager");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void removeTask(Task item) throws TaskNotSelectedException {
        if (item == null) {
            throw new TaskNotSelectedException();
        } else if (ConfirmBox.display("Confirm", "Do you want to remove this task?"))
                taskList.getItems().remove(item);
    }

    public void addTask(Task item) {
        taskList.getItems().add(item);
    }

    public void showNotify(Task task) {
        AlertBox.display(task.getName(), task.toString());
    }

}

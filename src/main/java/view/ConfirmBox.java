package view;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ConfirmBox {

    private static boolean result;

    public static boolean display(String title, String message) {

        Stage stage = new Stage();
        stage.initStyle(StageStyle.UTILITY);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle(title);

        Label label = new Label();
        label.setText(message);

        Button yesButton = new Button();
        yesButton.setText("YES");
        yesButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                result = true;
                stage.close();
            }
        });
        Button noButton = new Button();
        noButton.setText("NO");
        noButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                result = false;
                stage.close();
            }
        });

        VBox layout = new VBox(10);
        HBox buttons = new HBox(30);
        buttons.getChildren().addAll(yesButton, noButton);
        buttons.setAlignment(Pos.CENTER);
        layout.getChildren().addAll(label, buttons);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout, 250, 70);
        stage.setScene(scene);
        stage.setResizable(false);

        stage.showAndWait();
        return result;
    }
}
